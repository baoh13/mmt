﻿namespace Application.AppSetting
{
    public class AppSettings
    {
        public string CustomerDetailsServiceBaseUrl { get; set; }
        public string CustomerDetailsServiceCode { get; set; }
    }
}
